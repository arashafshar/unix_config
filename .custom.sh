
export PS1='\[\033[0;32m\]\h\[\033[0;33m\] :$\[\033[00m\] '

export ALTERNATE_EDITOR=""
export EDITOR=vim

xrandr --output DVI-1 --auto --left-of DVI-0

export TERM=xterm-256color

alias v='vim --servername VIM'
# So I can use ctrl-s in vim for saving instead of sending the XOFF signal
stty -ixon

alias cdg="cd /media/linuxstorage/git"

