#! /bin/bash

apt-get update
apt-get upgrade
apt-get autoremove

apt-get install vim
apt-get install build-essential cmake libtool autoconf

apt-get install python-pip
pip install powerline-status
pushd /tmp
git clone https://github.com/powerline/fonts
pushd fonts
./install.sh
popd
popd

# open vim and run :PlugInstall then:
apt-get install python-dev
pushd ~/.vim/plugged/YouCompleteMe/
./install.py --clang-completer
popd

echo "source ~/.custom.sh" >> ~/.bashrc

apt-get install i3 i3lock i3status


apt-get install gnuplot

# pushd /tmp
# wget http://users.phys.psu.edu/~collins/software/latexmk-jcc/latexmk-443a.zip
# unzip latexmk-443a.zip
# pushd latexmk
apt-get install latexmk

# Install solarized color scheme

