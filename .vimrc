set nocompatible
filetype plugin on
syntax on

" Load vim-plug
if empty(glob("~/.vim/autoload/plug.vim"))
    execute '!curl -fLo ~/.vim/autoload/plug.vim https://raw.github.com/junegunn/vim-plug/master/plug.vim'
    endif

call plug#begin('~/.vim/plugged')

Plug 'bronson/vim-trailing-whitespace'
Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer' }
Plug 'scrooloose/syntastic'

Plug 'lervag/vimtex'
Plug 'sirver/ultisnips'
Plug 'honza/vim-snippets'
Plug 'kien/ctrlp.vim'

Plug 'vimwiki/vimwiki'

Plug 'altercation/vim-colors-solarized'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

call plug#end()

" --------------- vimwiki
let g:vimwiki_list = [{'path':'~/Dropbox/notes/', 'path_html':'~/Dropbox/notes/html/'}]

" --------------- ctrlp
let g:ctrlp_max_height = 30
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=*/coverage/*

" --------------- vimtex
let g:vimtex_latexmk_options = '-pdf -pvc'
if !exists('g:ycm_semantic_triggers')
    let g:ycm_semantic_triggers = {}
endif
let g:ycm_semantic_triggers.tex = [
            \ 're!\\[A-Za-z]*(ref|cite)[A-Za-z]*([^]]*])?{([^}]*, ?)*'
            \ ]

let g:vimtex_complete_recursive_bib = 1
let g:vimtex_quickfix_ignore_all_warnings = 1

" " --------------- ag and ack config
" let g:unite_source_history_yank_enable = 1
" try
" 	let g:unite_source_rec_async_command='ag --nocolor --nogroup -g ""'
" 	call unite#filters#matcher_default#use(['matcher_fuzzy'])
" catch
" endtry
" " search a file in the filetree
" nnoremap <space><space> :split<cr> :<C-u>Unite -start-insert file_rec/async<cr>
" " reset not it is <C-l> normally
" :nnoremap <space>r <Plug>(unite_restart)
"
"
" nmap <C-l> :Ag <c-r>=expand("<cword>")<cr><cr>
" nnoremap <space>/ :Ag
"
" ---------------- theme
" set background=dark
" colorscheme badwolf

" Color scheme
" " mkdir -p ~/.vim/colors && cd ~/.vim/colors
" " wget -O wombat256mod.vim http://www.vim.org/scripts/download_script.php?src_id=13400
" set t_Co=256
" color wombat256mod
set t_Co=256
syntax enable
set background=dark
colorscheme solarized

" ----------------- Status bar
" if has("mac")
"     set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [POS=%l,%v][%p%%]\ %{strftime(\"%d/%m/%y\ -\ %H:%M\")}
"     set laststatus=2
" else
"     " pip install powerline
"     python from powerline.vim import setup as powerline_setup
"     python powerline_setup()
"     python del powerline_setup
"     " Always show statusline
"     set laststatus=2
"     " Use 256 colours (Use this setting only if your terminal supports 256 colours)
"     set t_Co=256
" endif

" --------------------- airline
set laststatus=2
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
" if !exists('g:airline_symbols')
"    let g:airline_symbols = {}
" endif


" unicode symbols
" let g:airline_left_sep = '»'
" let g:airline_left_sep = '▶'
" let g:airline_right_sep = '«'
" let g:airline_right_sep = '◀'
" let g:airline_symbols.linenr = '␊'
" let g:airline_symbols.linenr = '␤'
" let g:airline_symbols.linenr = '¶'
" let g:airline_symbols.branch = '⎇'
" let g:airline_symbols.paste = 'ρ'
" let g:airline_symbols.paste = 'Þ'
" let g:airline_symbols.paste = '∥'
" let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
" let g:airline_left_sep = ''
" let g:airline_left_alt_sep = ''
" let g:airline_right_sep = ''
" let g:airline_right_alt_sep = ''
" let g:airline_symbols.branch = ''
" let g:airline_symbols.readonly = ''
" let g:airline_symbols.linenr = ''

" ---------------- YouCompleteMe
let g:ycm_server_keep_logfiles = 1
let g:ycm_autoclose_preview_window_after_completion = 1
nnoremap <leader>d :YcmCompleter GoTo<CR>
" map <leader>d  :YcmCompleter GoToDefinitionElseDeclaration<CR>
let g:ycm_confirm_extra_conf = 0
"use omnicomplete whenever there's no completion engine in youcompleteme (for
"example, in the case of PHP)
set omnifunc=syntaxcomplete#Complete
"python with virtualenv support
py << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
  project_base_dir = os.environ['VIRTUAL_ENV']
  activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
  execfile(activate_this, dict(__file__=activate_this))
EOF




" " UltiSnips completion function that tries to expand a snippet. If there's no
" " snippet for expanding, it checks for completion window and if it's
" " shown, selects first element. If there's no completion window it tries to
" " jump to next placeholder. If there's no placeholder it just returns TAB key
" function! g:UltiSnips_Complete()
"     call UltiSnips#ExpandSnippet()
"     if g:ulti_expand_res == 0
"         if pumvisible()
"             return "\<C-n>"
"         else
"             call UltiSnips#JumpForwards()
"             if g:ulti_jump_forwards_res == 0
"                 return "\<TAB>"
"             endif
"         endif
"     endif
"     return ""
" endfunction
"
" au BufEnter * exec "inoremap <buffer> <silent> " . g:UltiSnipsExpandTrigger . " <C-R>=g:UltiSnips_Complete()<cr>"
" " let g:UltiSnipsJumpForwardTrigger="<tab>"
" " let g:UltiSnipsListSnippets="<c-e>"
" " this mapping Enter key to <C-y> to chose the current highlight item
" " and close the selection list, same as other IDEs.
" " CONFLICT with some plugins like tpope/Endwise
" inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"


" ---------------- Ultisnippet
" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-space>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

" If you want :UltiSnipsEdit to split your window.
" let g:UltiSnipsEditSplit="vertical"


" ---------------- Syntastic
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
" let g:syntastic_python_checkers=['pylint']
let g:syntastic_python_checkers=['flake8']
let g:syntastic_python_flake8_args='--ignore=F401'
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" ------------------ General
" Enable syntax highlighting
" You need to reload this file for the change to apply
filetype off
filetype plugin indent on
syntax on
" backspace deletes newline, etc
set backspace=2
" we can have unsaved hidden buffers
set hidden
" when scrolling down or up, keep the cursor 8 line above or below
set scrolloff=8
" cursor can move to illegal positions
set virtualedit=all
" disable encryption :X
set key=
" contents of the clipboard can be pasted using p/P
set clipboard+=unnamed
" set clipboard=unnamed
" file changes outside vim are auto loaded
set autoread
" Swich between terminal and vim
noremap <c-d> :sh<cr>

" not sure what it does!!!
syn on se title
" Real programmers don't use TABs but spaces
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab
" show matching braces, etc
set showmatch
" show line numbers
set number
" show line numbers relative to current position
set relativenumber
" Showing line numbers and length
set tw=79   " width of document (used by gd)
set nowrap  " don't automatically wrap on load
"" set fo-=t   " don't automatically wrap text when typing

" easier formatting of paragraphs
vmap Q gq
nmap Q gqap
" set a marker on 79s column
set colorcolumn=120
" set the color of the 80th colum
highlight ColorColumn ctermbg=darkgray
" Make search case insensitive
"" set hlsearch
set incsearch
set ignorecase
set smartcase

" enable reading vimrc form the current directory
set exrc
set secure
" show the line and column numde of the cursor
set ruler

map <F5> :make<CR>
nnoremap <C-H> :buffers<CR>:buffer<Space>
" nnoremap <C-M> :bd<CR>
imap jk <Esc>
imap kj <Esc>

nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <right> <nop>
nnoremap <left> <nop>

nnoremap <PageDown> <nop>
nnoremap <PageUp> <nop>

nnoremap j gj
nnoremap k gk

" Automatic reloading of .vimrc
autocmd! bufwritepost .vimrc source %

" toggle whether the indentation should be preserved when pasting from other
" apps, clipboard, etc. Works in insert mode.
set pastetoggle=<F2>

" open every buffer in its own tab
" au BufAdd,BufNewFile * nested tab sball

" Rebind <Leader> key
let mapleader = ","


" Bind nohl
" Removes highlight of your last search
" ``<C>`` stands for ``CTRL`` and therefore ``<C-n>`` stands for ``CTRL+n``
"" noremap <C-n> :nohl<CR>
"" vnoremap <C-n> :nohl<CR>
"" inoremap <C-n> :nohl<CR>


" Quicksave command
noremap <C-S> :update<CR>
vnoremap <C-S> <C-C>:update<CR>
inoremap <C-S> <C-O>:update<CR>


" Quick quit command
noremap <Leader>e :quit<CR>  " Quit current window
noremap <Leader>E :qa!<CR>   " Quit all windows


" bind Ctrl+<movement> keys to move around the windows, instead of using
" Ctrl+w + <movement>
" Every unnecessary keystroke that can be saved is good for your health :)
" map <c-j> <c-w>j
" map <c-k> <c-w>k
" map <c-l> <c-w>l
" map <c-h> <c-w>h


" easier moving between tabs
map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>


" map sort function to a key
vnoremap <Leader>s :sort<CR>


" easier moving of code blocks
" Try to go into visual mode (v), thenselect several lines of code here and
" then press ``>`` several times.
vnoremap < <gv  " better indentation
vnoremap > >gv  " better indentation


" Show whitespace
" MUST be inserted BEFORE the colorscheme command
"" autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
"" au InsertLeave * match ExtraWhitespace /\s\+$/


" Useful settings
set history=700
set undolevels=700


" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
" set nobackup
" set nowritebackup
" set noswapfile


